# cobbled paths

from figlet2svgbob, creating contextual stroke font ready to plot.

this is called _cobbled paths_ as a reminder of the permeability between the discrete and the continuous.
smooth connected paths are made out of an extremely restrictive grid, like multiple blocky stones form a tortuous path.

## dependencies

* python
* flask
* figlet
* svgbob
* [vpype](https://github.com/abey79/vpype), for converting to HPGL

## installation

Firstly [download](https://gitlab.constantvzw.org/osp/tools.cobbled-paths/-/archive/master/tools.cobbled-paths-master.zip) this repository, or clone it: `git clone git@gitlab.constantvzw.org:osp/tools.cobbled-paths.git`

### Python

The interface uses python and require python 3, find information on how to [install python here](https://www.python.org/downloads/)

Then, use pip to install the requirements with: 

```pip install -r requirements.txt```

### Figlet

On **linux**, install it with package manager: `apt install figlet`, or `pacman -S figlet`

On **Mac**, you can either download a pre-built binary from the [figlet website](http://www.figlet.org/), or install it with [Homebrew](https://brew.sh/): `brew install figlet`.


### To install SVG Bob

Ensure you have rust installed, [installation instructions here](https://www.rust-lang.org/tools/install)

Then `cargo install svgbob_cli`

### vpype

Installation of vpype is optional. It is used to convert the SVG files to HPGL.

Please follow the [installation instructions](https://github.com/abey79/vpype#installation)

## font database

* figlet offical ftp at <ftp://ftp.figlet.org>
  * `ours` the original default font made by the developer and given with the program, in early 1993
  * `contributed` fonts made by figlet amateur and submitted to the official figlet ftp, from before 1993 to 2005
    * `c64` are fonts that are only made of the `#` character, the most black ascii char, is if it was a pixel
    * `bdffont` are fonts automatically made using bdf2figlet, converting [Glyph Bitmap Distribution Format](https://en.wikipedia.org/wiki/Glyph_Bitmap_Distribution_Format), often from adobe to `c64` like font.
* figlet font library of JavE (a free Ascii Editor), <http://www.jave.de/figlet/fonts.html>. those also include the figlet ftp native, they where sorted in order to keep only the uniques ones.


<!-- 
## todo

* vpype simplify
* reload bug in draw

* image tab
* group all js in js folder
* default dic in one place

* input listen when opening page (for browser history remember)
* show font-info file

-->
