import sys

"""
  Generates glyphs entries for an FLF file.
  It does not generate the file header.
  
  Run like: make_flf.py [glyph_height]
"""

height = int(sys.argv[1])

flf = ''

for chr_code in range(32, 126):
  for line in range(height):
    flf += " "

    if line == 0:
      flf += chr(chr_code)
    else:
      flf += " "
      
    flf += "@"

    if line == height-1:
      flf += "@"

    flf += "\n"

print(flf)