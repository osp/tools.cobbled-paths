from hpgl_multipen_output import HpglMultipenOutput
import subprocess
# from hpgl_output_adapted import HpglOutputAdapted

def svgToHPGL (path, speed=1, penCount=8, force=2):

    subprocess.call(['inkscape', 
        '-f', path,
        '--verb', 'EditSelectAll',
        '--verb', 'ObjectToPath',
        '--verb', 'FileSave',
        '--verb', 'FileQuit' ])

    e = HpglMultipenOutput()
    e.affect([
        '--orientation', '270',
        '--force', '0',
        '--overcut', '0',
        '--precut', 'false',
        '--flat', '8',
        '--toolOffset', '0',
        '--autoAlign', 'false',
        '--speed', str(speed),
        '--force', str(force),
        path], False)

    return e.hpgl