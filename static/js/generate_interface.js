let button_pad = document.getElementById('button-pad');
let button_svg = document.getElementById('button-svg');
let svg_iframe = document.getElementById('svg-iframe');
let pad_iframe = document.getElementById('pad-iframe');

let new_url = new URL(svg_iframe.src);

function updateGET(frame, param, value){
    // object from GET parameters
    let [base_src, params_src] = frame.src.split("?");
    let params = new URLSearchParams(params_src);
    // update param
    params.set(param, value);
    // reconstituate URL
    let new_src = base_src + "?" + params.toString();
    // set and refresh
    frame.src = new_src;
}

// --- pad go button
if(button_pad){
    button_pad.addEventListener('click', function(){
        let input = document.getElementById(button_pad.dataset.use);
        let value = input.value;
    
        let pad_src = pad_iframe.src;
        pad_src = pad_src.split('-');
        pad_src[pad_src.length-1] = value;
        pad_src = pad_src.join('-');
        pad_iframe.src = pad_src;
    
        let svg_src = svg_iframe.src;
        svg_src = svg_src.split('/');
        svg_src[svg_src.length-1] = value;
        svg_src = svg_src.join('/');
    
        new_url = new URL(svg_src);
        svg_iframe.src = new_url;
        document.getElementById('main').classList.add("reload");
    });
}

// --- svg generation button
if(button_svg){
    button_svg.addEventListener('click', function(){
        svg_iframe.src = new_url;
        document.getElementById('main').classList.add("reload");
    });
}

// --- get-input but on the pad and checkbox but on the pad
let inputs = document.getElementsByClassName('get-input');

for(let input of inputs){
    input.addEventListener('change', function(){
    let frame = document.getElementById(input.dataset.frame);
    const url = new URL(frame.src);

    if(input.type == 'checkbox'){
        url.searchParams.set(input.dataset.name, input.checked);
    }
    else{
        url.searchParams.set(input.dataset.name, input.value);
    }
    new_url = url;
    });
}

svg_iframe.addEventListener("load", function() {
    document.getElementById('main').classList.remove("reload");
});