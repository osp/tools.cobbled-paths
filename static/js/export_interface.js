
function toggle_class(classname, val){
  if(val){
      document.body.classList.add(classname);
  }
  else{
      document.body.classList.remove(classname);
  }
}
let body_class_checkboxes = document.getElementsByClassName("body-class-check");
for(let checkbox of body_class_checkboxes){
    let classname = checkbox.value;
    checkbox.addEventListener('input', function(){
        toggle_class(classname, checkbox.checked);
    });
    toggle_class(classname, checkbox.checked);
}

let save_button_svg = document.getElementById('save-svg');
save_button_svg.addEventListener('click', function(){
  let url = document.URL,
  parts = url.split('/'),
  name = parts[parts.length-1],
  svg_url = '/svg/' + name,
  a = document.createElement('a');
  a.href = svg_url;
  a.setAttribute('download', 'download');
  if (document.createEvent) {
    const event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    a.dispatchEvent(event);
  }
  else {
    a.click();
  }
});

let save_button_hpgl = document.getElementById('save-hpgl');
save_button_hpgl.addEventListener('click', function () {
  let url = document.URL,
      parts = url.split('/'),
      name = parts[parts.length-1],
      hpgl_url = '/hpgl/' + name,
      a = document.createElement('a');
  a.href = hpgl_url;
  a.setAttribute('download', 'download');
  if (document.createEvent) {
    const event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    a.dispatchEvent(event);
  }
  else {
    a.click();
  }
});
